﻿using BDMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BDMS.Controllers
{
    public class HospitalController : Controller
    {
        BDMSContext context = new BDMSContext();

        /// <summary>
        /// get list of available hospitals
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Message = "Your application description page.";
            HospitalModel hospitalModel = new HospitalModel();
            hospitalModel.Hospitals = context.Hospitals.ToList();

            if (TempData["Message"] != null && !string.IsNullOrWhiteSpace(TempData["Message"].ToString()))
            {
                hospitalModel.Message = TempData["Message"].ToString();
                hospitalModel.MessageType = TempData["MessageType"].ToString();
            }
            return View(hospitalModel);
        }

        [HttpGet]
        public ActionResult Detail(int id = 0)
        {
            HospitalModel hospitalModel = new HospitalModel();
            if (id > 0)
            {
                hospitalModel.CurrentHospital = context.Hospitals.FirstOrDefault(hospital => hospital.Id == id);
                return View(hospitalModel);
            }
            else
            {
                return RedirectToAction("Index", "Hospital");
            }
        }

        [HttpGet]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                var currentHospital = context.Hospitals.FirstOrDefault(hospital => hospital.Id == id);
                context.HosptalBloodDonors.RemoveRange(currentHospital.HosptalBloodDonors);
                context.HospitalBloodGroups.RemoveRange(currentHospital.HospitalBloodGroups);
                context.Hospitals.Remove(currentHospital);
                context.SaveChanges();
                TempData["Message"] = "Hospital Details deleted Sucessfully.";
                TempData["MessageType"] = "info";
            }
            catch (Exception)
            {
                TempData["Message"] = "There is some issue in deleting Hospital Details.";
                TempData["MessageType"] = "info";
            }
            
            return RedirectToAction("Index","Hospital");

        }

        [HttpGet]
        public ActionResult Create(int id = 0)
        {
            HospitalModel hospitalModel = new HospitalModel();
            hospitalModel.BloodGroups = context.BloodGroups.ToList();
            if (id > 0)
            {
                hospitalModel.CurrentHospital = context.Hospitals.FirstOrDefault(hospital => hospital.Id == id);
            }
            else
            {
                hospitalModel.CurrentHospital = new Hospital();
            }


            return View(hospitalModel);

        }

        [HttpPost]
        public ActionResult Create(Hospital currentHospital, int[] SelectedBloodGroup)
        {
            try
            {
                if (currentHospital.Id == 0)
                {
                    currentHospital.CreatedOn = DateTime.Now;
                    currentHospital = context.Hospitals.Add(currentHospital);
                    context.SaveChanges();
                    foreach (var item in SelectedBloodGroup)
                    {
                        HospitalBloodGroup hospitalBloodGroup = new HospitalBloodGroup();
                        hospitalBloodGroup.HospitalId = currentHospital.Id;
                        hospitalBloodGroup.BloodGroupId = item;
                        context.HospitalBloodGroups.Add(hospitalBloodGroup);
                        context.SaveChanges();
                    }

                    TempData["Message"] = "Hospital Details Saved Sucessfully.";
                    TempData["MessageType"] = "success";
                }
                else
                {

                    var selectedHospital = context.Hospitals.FirstOrDefault(hospital => hospital.Id == currentHospital.Id);
                    selectedHospital.Name = currentHospital.Name;                 
                    context.SaveChanges();

                    List<HosptalBloodDonor> donors = new List<HosptalBloodDonor>();
                    foreach (var item in SelectedBloodGroup)
                    {
                        var existingDonor = selectedHospital.HosptalBloodDonors.ToList().FirstOrDefault(bd => bd.BloodGroupid == item);
                        if (existingDonor != null)
                            donors.Add(existingDonor);
                    }

                    context.HosptalBloodDonors.RemoveRange(donors);
                    context.HospitalBloodGroups.RemoveRange(selectedHospital.HospitalBloodGroups);
                    context.SaveChanges();

                    foreach (var item in SelectedBloodGroup)
                    {
                        HospitalBloodGroup hospitalBloodGroup = new HospitalBloodGroup();
                        hospitalBloodGroup.HospitalId = currentHospital.Id;
                        hospitalBloodGroup.BloodGroupId = item;
                        context.HospitalBloodGroups.Add(hospitalBloodGroup);
                        context.SaveChanges();
                    }


                    TempData["Message"] = "Hospital Details Saved Sucessfully.";
                    TempData["MessageType"] = "info";
                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "There is some issue in saving Hospital Details.";
                TempData["MessageType"] = "danger";
            }


            return RedirectToAction("Index", "Hospital");

        }
    }
}