﻿using BDMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BDMS.Controllers
{
    public class DonorController : Controller
    {
        BDMSContext context = new BDMSContext();

        /// <summary>
        /// get list of available hospitals
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Message = "Your application description page.";
            DonorModel donorModel = new DonorModel();
            donorModel.Donors = context.Donors.ToList();

            if (TempData["Message"] != null && !string.IsNullOrWhiteSpace(TempData["Message"].ToString()))
            {
                donorModel.Message = TempData["Message"].ToString();
                donorModel.MessageType = TempData["MessageType"].ToString();
            }
            return View(donorModel);
        }

        [HttpGet]
        public ActionResult Detail(int id = 0)
        {
            DonorModel donorModel = new DonorModel();
            if (id > 0)
            {
                donorModel.CurrentDonor = context.Donors.Include("HosptalBloodDonors").FirstOrDefault(donor => donor.Id == id);
                return View(donorModel);
            }
            else
            {
                return RedirectToAction("Index", "Donor");
            }
        }

        [HttpGet]
        public ActionResult Delete(int id = 0)
        {
            try
            {
                var currentDonor = context.Donors.FirstOrDefault(donor => donor.Id == id);
                context.HosptalBloodDonors.RemoveRange(currentDonor.HosptalBloodDonors);
                context.Donors.Remove(currentDonor);
                context.SaveChanges();
                TempData["Message"] = "Donor Details deleted Sucessfully.";
                TempData["MessageType"] = "info";
            }
            catch (Exception)
            {
                TempData["Message"] = "There is some issue in deleting donor Details.";
                TempData["MessageType"] = "info";
            }

            return RedirectToAction("Index", "Donor");

        }

        [HttpGet]
        public ActionResult Create(int id = 0)
        {
            DonorModel donorModel = new DonorModel();
            donorModel.SeletedHospitalItems = new List<SelectListItem>();
            donorModel.BloodGroups = context.BloodGroups.ToList().Select(bg => new SelectListItem() { Text = bg.Name, Value = bg.Id.ToString() }).ToList();
            if (id > 0)
            {
                donorModel.CurrentDonor = context.Donors.FirstOrDefault(donor => donor.Id == id);
                donorModel.SelectedBloodGroup = donorModel.CurrentDonor.HosptalBloodDonors.FirstOrDefault().BloodGroup.Id;
                
                foreach (var item in context.HospitalBloodGroups.Include("Hospital").Where(hbg => hbg.BloodGroupId == donorModel.SelectedBloodGroup))
                {
                    donorModel.SeletedHospitalItems.Add(new SelectListItem()
                    {
                        Text = item.Hospital.Name,
                        Value = item.HospitalId.ToString(),
                        Selected= donorModel.CurrentDonor.HosptalBloodDonors.ToList().Exists(bd=>bd.HospitalId==item.HospitalId)
                    });
                }
            }
            else
            {
                donorModel.CurrentDonor = new Donor();
            }


            return View(donorModel);

        }

        [HttpPost]
        public ActionResult Create(Donor currentDonor, int[] SelectedHospitals, int SelectedBloodGroup)
        {
            try
            {
                if (currentDonor.Id == 0)
                {
                    currentDonor.CreatedOn = DateTime.Now;
                    currentDonor = context.Donors.Add(currentDonor);
                    context.SaveChanges();
                    foreach (var item in SelectedHospitals)
                    {
                        HosptalBloodDonor hosptalBloodDonor = new HosptalBloodDonor();
                        hosptalBloodDonor.HospitalId = item;
                        hosptalBloodDonor.DonorId = currentDonor.Id;
                        hosptalBloodDonor.BloodGroupid = SelectedBloodGroup;
                        context.HosptalBloodDonors.Add(hosptalBloodDonor);
                        context.SaveChanges();
                    }

                    TempData["Message"] = "Donor Details Saved Sucessfully.";
                    TempData["MessageType"] = "success";
                }
                else
                {

                    var selectedDonor = context.Donors.FirstOrDefault(hospital => hospital.Id == currentDonor.Id);
                    selectedDonor.Name = currentDonor.Name;
                    context.SaveChanges();

                    context.HosptalBloodDonors.RemoveRange(selectedDonor.HosptalBloodDonors);
                    context.SaveChanges();

                    foreach (var item in SelectedHospitals)
                    {
                        HosptalBloodDonor hosptalBloodDonor = new HosptalBloodDonor();
                        hosptalBloodDonor.HospitalId = item;
                        hosptalBloodDonor.DonorId = currentDonor.Id;
                        hosptalBloodDonor.BloodGroupid = SelectedBloodGroup;
                        context.HosptalBloodDonors.Add(hosptalBloodDonor);
                        context.SaveChanges();
                    }


                    TempData["Message"] = "Donor Details Saved Sucessfully.";
                    TempData["MessageType"] = "info";
                }

            }
            catch (Exception ex)
            {
                TempData["Message"] = "There is some issue in saving Donor Details.";
                TempData["MessageType"] = "danger";
            }


            return RedirectToAction("Index", "Donor");

        }

        public JsonResult GetHospitalForBloodGroup(int id = 0)
        {
            List<CustomItem> availableHospitals = new List<CustomItem>();
            try
            {
                int bloodGroupId = id;
                var hospitalBloodGroups = context.HospitalBloodGroups.Include("Hospital").Where(hbg => hbg.BloodGroupId == bloodGroupId);
                foreach (var item in hospitalBloodGroups)
                {
                    availableHospitals.Add(new CustomItem() { Name = item.Hospital.Name, Id = item.Hospital.Id });
                }
            }
            catch (Exception)
            {

            }

            return Json(availableHospitals, JsonRequestBehavior.AllowGet);
        }
    }
}