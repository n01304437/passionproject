﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BDMS.Models
{
    public class DonorModel : BaseModel
    {
        public List<Donor> Donors { get; set; }

        public Donor CurrentDonor { get; set; }

        [Required(ErrorMessage = "Please select the blood group.")]
        public int SelectedBloodGroup { get; set; }

        public List<SelectListItem> BloodGroups { get; set; }

        public List<SelectListItem> SeletedHospitalItems { get; set; }

        [Required(ErrorMessage = "Please select the belongs hospital to blood group.")]
        public List<int> SelectedHospitals { get; set; }
    }

    public class CustomItem
    {
        public int Id { get; set; }

        public string Name { get; set; } 
    }
}