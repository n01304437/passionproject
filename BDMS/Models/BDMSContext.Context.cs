﻿namespace BDMS
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class BDMSContext : DbContext
    {
        public BDMSContext()
            : base("name=Hospital_Context")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<BloodGroup> BloodGroups { get; set; }
        public virtual DbSet<Donor> Donors { get; set; }
        public virtual DbSet<HospitalBloodGroup> HospitalBloodGroups { get; set; }
        public virtual DbSet<Hospital> Hospitals { get; set; }
        public virtual DbSet<HosptalBloodDonor> HosptalBloodDonors { get; set; }
    }
}
