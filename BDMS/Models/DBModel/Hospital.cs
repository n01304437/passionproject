//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BDMS
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Hospital
    {
        public Hospital()
        {
            this.HospitalBloodGroups = new HashSet<HospitalBloodGroup>();
            this.HosptalBloodDonors = new HashSet<HosptalBloodDonor>();
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter hospital name.")]
        public string Name { get; set; }

        public System.DateTime CreatedOn { get; set; }

        public virtual ICollection<HospitalBloodGroup> HospitalBloodGroups { get; set; }

        public virtual ICollection<HosptalBloodDonor> HosptalBloodDonors { get; set; }
    }
}
