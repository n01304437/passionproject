﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BDMS.Models
{
    public class BaseModel
    {
        public string Message { get; set; }

        public string MessageType { get; set; }
    }
}