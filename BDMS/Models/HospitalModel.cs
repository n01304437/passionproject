﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BDMS.Models
{
    public class HospitalModel : BaseModel
    {
        public List<Hospital> Hospitals { get; set; }

        public Hospital CurrentHospital { get; set; }

        public List<BloodGroup> BloodGroups { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select required blood groups.")]
        public string SelectedBloodGroups { get; set; }

    }
}